var gulp = require('gulp'),
    concat = require('gulp-concat');


gulp.task('make', function() {
    return gulp.src([
        'src/prefix.js',
        'src/module/**/*.js',
        'src/suffix.js',
    ]).pipe(concat('dt.js'))
      .pipe(gulp.dest('./dist/'));
});
