(function(global, core) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = core() :
    typeof define === 'function' && define.amd ? define(core) :
    (global.dt = core());
}(this, function() { 'use strict';

var vm = {};

vm.cookies = function() {
    var vm = {};

    vm.set = function(name, value, days_before_expiry) {
        /*
         * Sets a cookie with the name-value pair name=value. Expiry can be set
         * by specifying days_before_expiry defaulting to one week.
         */
        var expires = new Date();

        if(days_before_expiry == undefined)
            days_before_expiry = 7;

        expires.setDate(expires.getDate() + days_before_expiry);
        expires = expires.toUTCString();

        document.cookie = name + '=' + value + '; expires=' + expires + ';';
    }

    vm.get = function(cname) {
        /*
         * Tries to find and return a cookie with the name cname. Returns an
         * empty string if not found.
         * Almost 100% copy from w3schools' https://www.w3schools.com/js/js_cookies.asp
         */
        var name = cname + '=';
        var cookie_split = document.cookie.split(';');

        for(var i = 0; i < cookie_split.length; i++) {
            var cookie = cookie_split[i];

            while(cookie.charAt(0) == ' ')
                cookie = cookie.substring(1);

            if(cookie.indexOf(name) == 0)
                return cookie.substring(name.length, cookie.length);
        }

        return '';
    }

    vm.register = function(path, component) {
        vm.routes.push({ path: path, component: component });
    }

    return vm;
}();

vm.helper = function() {
    var vm = {
    };

    vm.register = function(path, component) {
        vm.routes.push({ path: path, component: component });
    }
    vm.valign = function(container, element, offset) {
        var center = (container.clientHeight / 2) - (element.clientHeight / 2);

        if(offset === undefined)
            offset = 0;

        element.style.marginTop = (center - (offset / 2)) + 'px';
    }

    return vm;
}();

vm.http = function() {
    var vm = {}

    vm.request = function(url, method, data=null) {
        /*
         * Performs an http {method} request to {url} with {data}
         */
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();

            method = method.toUpperCase();

            xhr.open(method, url);

            if(method == 'POST')
                xhr.setRequestHeader('Content-Type', 'application/json');

            xhr.onload = function() {
                if(this.status >= 200 && this.status < 300)
                    resolve(JSON.parse(xhr.response));
                else
                    reject({
                        status: this.status,
                        statusText: xhr.statusText
                    });
            };

            xhr.onerror = function() {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                })    
            };

            if(data != null)
                data = JSON.stringify(data);

            xhr.send(data);
        }); 
    }

    // Shortcut for GET requests
    vm.get = (url) => vm.request(url, 'GET'); 
    // Shortcut for POST requests
    vm.post = (url, data) => vm.request(url, 'POST', data); 
    // Shortcut for DELETE requests
    vm.del = (url) => vm.request(url, 'DELETE'); 

    return vm; 
}();

vm.router = function() {
    var vm = {
        routes: [],
    };

    vm.register = function(path, component) {
        vm.routes.push({ path: path, component: component });
    }

    return vm;
}();

return vm;

}));
