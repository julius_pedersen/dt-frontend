http: class http {
    static request(url, method, data=null) {
        /*
         * Performs an http {method} request to {url} with {data}
         */
        return new Promise(function(resolve, reject) {
            method = method.toUpperCase();

            var xhr = new XMLHttpRequest();
            xhr.open(method, url);

            if(['POST', 'PUT', 'PATCH'].indexOf(method) > -1) {
                xhr.setRequestHeader('Content-Type', 'application/json');

                let csrftoken = Cookies.get('csrf_token');
                if('csrf_token' in vm)
                    xhr.setRequestHeader('X-CSRFToken', csrftoken);
            }

            xhr.onload = function() {
                let result = JSON.parse(xhr.response);
                if('csrf_token' in result) {
                    Cookies.set('csrf_token', result.csrf_token);

                if(this.status >= 200 && this.status < 300)
                    resolve(result);
                else
                    reject({
                        status: this.status,
                        statusText: xhr.statusText
                    });
            };

            xhr.onerror = function() {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                })    
            };

            if(data != null)
                data = JSON.stringify(data);

            xhr.send(data);
        });
    }

    // Shortcut for GET requests
    static get(url) { return this.request(url, 'GET'); }
    // Shortcut for POST requests
    static post(url, data) { return this.request(url, 'POST', data); }
    // Shortcut for PUT requests
    static put(url, data) { vm.request(url, 'PUT', data); }
    // Shortcut for DELETE requests
    static del(url) { vm.request(url, 'DELETE'); }
},
