vm.router = function() {
    var vm = {
        routes: [],
    };

    vm.register = function(path, component) {
        vm.routes.push({ path: path, component: component });
    }

    return vm;
}();
